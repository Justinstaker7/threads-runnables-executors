package week8;



    public class MyApp implements Runnable {

        private String userName; //variable for user name
        private String password; //variable for password
        private int accountNumber; //variable for accountNumber
        private String onlineServer; //variable for server (not a real server just an example)

    //method for credential variables
    public MyApp(String userName, String password, int accountNumber){
        this.userName = userName;
        this.password = password;
        this.accountNumber = accountNumber;
    }

    //method for server
    public MyApp(String onlineServer){
        this.onlineServer = onlineServer;
    }

    //output the runnables in the console. They should only run if correct credentials and server are used
    public void run(){
        System.out.println("Opening " + userName + "'s account using password " + password +
                " and account number " + accountNumber + ".....");

        //data validation for username
        if(userName == ""){
            System.out.println("Connection Successful");
        } else if(userName == null){
            System.out.println("Connection Unsuccessful");
        } else{
            System.out.println("Connection Unsuccessful");
        }
        //data validation for password
        if(password == ""){
            System.out.println("Connection Successful");
        } else if(password == null){
            System.out.println("Connection Unsuccessful");
        } else {
            System.out.println("Connection Unsuccessful");
        }
        //data validation for accountNumber
        if(accountNumber > 0){
            System.out.println("Connection Successful");
        } else if (accountNumber == 0){
            System.out.println("Connection Unsuccessful");
        } else {
            System.out.println("Connection Unsuccessful");
        }


        //output that program is connecting to server
        System.out.println("Connecting to online server using server name " + onlineServer + "....");

        //data validation for online server
        if(onlineServer == "Abbey Road S1"){
            System.out.println("Connection Successful");
        } else if(onlineServer == null){
            System.out.println("Connection Unsuccessful");
        } else {
            System.out.println("Connection Unsuccessful");
        }

    }

}
