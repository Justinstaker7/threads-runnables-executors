package week8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {

        //thread pool object
        ExecutorService appService = Executors.newFixedThreadPool(5);

            //executables that are users entering in credentials
            MyApp executable1 = new MyApp("McCartneyPaul", "playBass", 111111);
            MyApp executable2 = new MyApp("LennonJohn", "$The-Talent$", 222222);
            MyApp executable3 = new MyApp("HarrisonGeorge", "StringShredder", 333333);
            MyApp executable4 = new MyApp("StarrRingo", "LOUDNOISEMAKER", 444444);

            //executable connecting to "server" (really just a string)
            MyApp serverConnection = new MyApp("Abbey Road S1");

            //execute the runnables
            appService.execute(executable1);
            appService.execute(executable2);
            appService.execute(executable3);
            appService.execute(executable4);
            appService.execute(serverConnection);

            appService.shutdown(); //stop execution


    }
}
